# multi-documentations

## Description

This repository contains several OpenAPI definintions.

It is created as a demo, to synchronize all these API definitions here
(in folder /docs), with API documentations deployed on [Bump.sh](https://bump.sh),
inside hub https://bump.sh/demo/hub/gitlab.

- petstore-api-source.yml ➡️ https://bump.sh/demo/hub/gitlab/doc/petstore
- restaurants-api-source.yml ➡️ https://bump.sh/demo/hub/gitlab/doc/restaurants
- train-travel-api-source.yml ➡️ https://bump.sh/demo/hub/gitlab/doc/train-travel
